all:
	make -C /lib/modules/$(shell uname -r)/build M=$(CURDIR) modules

load:
	@sudo dmesg -c >/dev/null
	@-sudo rmmod grant_cap_net_raw
	@sudo insmod grant_cap_net_raw.ko || (sudo dmesg -c; exit 1)
	@sudo dmesg -c

.PHONY: test
test:
	@make -C test && test/ec_test
