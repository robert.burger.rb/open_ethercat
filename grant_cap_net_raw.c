/* grant_cap_net_raw
 * Copyright (C) 2016 Florian Schmidt
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/cred.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/capability.h>

#define GRANT_CAP_NET_RAW_PROCFS_INFO "grant_cap_net_raw"

int grant_cap_net_raw_uids[32];
int grant_cap_net_raw_uid_count;
module_param_array(grant_cap_net_raw_uids, int, &grant_cap_net_raw_uid_count, 0660);

int grant_cap_net_raw_gids[32];
int grant_cap_net_raw_gid_count;
module_param_array(grant_cap_net_raw_gids, int, &grant_cap_net_raw_gid_count, 0660);

static struct proc_dir_entry* info_file = NULL;

#if !defined(KUIDT_INIT) // LINUX_VERSION_CODE < KERNEL_VERSION(3, 12, 0)
// old / suse backported kernels

#define my_get_id_val(a) ((int)a)
static int check_access_rights(void) {
	unsigned int i;
	uid_t uid = current_uid();
	gid_t gid = current_gid();
	
	// allow everyone
	if(grant_cap_net_raw_uid_count == 0 && grant_cap_net_raw_gid_count == 0) {
		return 1;
	}
			
 	// check whether this uid is mentioned or is in an "allowed" group
	for(i = 0; i < grant_cap_net_raw_uid_count; i++) {
		if(grant_cap_net_raw_uids[i] == uid) {
			return 1;
		}
	}
	
	for(i = 0; i < grant_cap_net_raw_gid_count; i++) {
		gid_t this_gid = grant_cap_net_raw_gids[i];
				
		if(this_gid == gid) {
			return 1;
		}
			
		if(in_group_p(this_gid) == 1) {
			return 1;
		}
	}
	
	return 0; // deny
}

#else // kernels like ~4.4 & ~4.12

#define my_get_id_val(a) ((int)a.val)
static int check_access_rights(void) {
	unsigned int i;
	kuid_t uid = current_uid();
	kgid_t gid = current_gid();

	if(grant_cap_net_raw_uid_count == 0 && grant_cap_net_raw_gid_count == 0) {
		// allow everyone
		return 1;
	}
	
 	// check whether this uid is mentioned or is in an "allowed" group
	for(i = 0; i < grant_cap_net_raw_uid_count; i++) {
		if(my_get_id_val(uid) == grant_cap_net_raw_uids[i]) {
			return 1;
		}
	}
	for(i = 0; i < grant_cap_net_raw_gid_count; i++) {
		kgid_t this_gid;
		this_gid.val = grant_cap_net_raw_gids[i];
			
		if(gid_eq(gid, this_gid)) {
			return 1;
		}
			
		if(in_group_p(this_gid) == 1) {
			return 1;
		}
	}
	
	return 0; // deny!
}
#endif

static int grant_cap_net_raw_show(struct seq_file* m, void* v) {
	struct cred* cred = NULL;

	if(capable(CAP_NET_RAW)) {
		seq_printf(m, "OK, had already.\n");
		return 0;
	}

	if(!check_access_rights()) {
		seq_printf(m, "ERROR, grant not allowed for user_id %d, group_id %d!\n",
			   my_get_id_val(current_uid()),
			   my_get_id_val(current_gid()));
		return 0;
	}
	
	// try to grant
	cred = (struct cred*)current_cred();
	cap_raise(cred->cap_effective, CAP_NET_RAW);
  
	// check again
	if(!capable(CAP_NET_RAW)) {
		seq_printf(m, "ERROR, grant failed!\n");
		return 0;
	}
  
	seq_printf(m, "OK, granted.\n");
	return 0;
}

static int grant_cap_net_raw_open(struct inode* inode, struct  file* file) {
	return single_open(file, grant_cap_net_raw_show, NULL);
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static const struct proc_ops grant_cap_net_raw_procfs_info_fops = {
	.proc_open = grant_cap_net_raw_open,
	.proc_read = seq_read,
	.proc_lseek = seq_lseek,
	.proc_release = single_release,
};
#else
static const struct file_operations grant_cap_net_raw_procfs_info_fops = {
	.owner = THIS_MODULE,
	.open = grant_cap_net_raw_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};
#endif

static int grant_cap_net_raw_init(void) {
	info_file = proc_create(GRANT_CAP_NET_RAW_PROCFS_INFO, 0444, NULL, &grant_cap_net_raw_procfs_info_fops);
	if(!info_file) {
		remove_proc_entry(GRANT_CAP_NET_RAW_PROCFS_INFO, NULL);
		printk(KERN_ALERT "grant_cap_net_raw: error: could not create /proc/" GRANT_CAP_NET_RAW_PROCFS_INFO " file!\n");
		return -ENOMEM;
	}

	if(!grant_cap_net_raw_uid_count && !grant_cap_net_raw_gid_count)
		printk(KERN_INFO "grant_cap_net_raw: no grant_cap_net_raw_uids or grant_cap_net_raw_gids specified. access will be granted to all\n");
	else {
		unsigned int i;
		if(grant_cap_net_raw_uid_count) {
			printk(KERN_INFO "grant_cap_net_raw: these user ids will be granted access: ");
			for(i = 0; i < grant_cap_net_raw_uid_count; i++) {
				if(i > 0)
					printk(KERN_CONT ", ");
				printk(KERN_CONT "%d", grant_cap_net_raw_uids[i]);
			}
			printk(KERN_CONT "\n");
		}
		if(grant_cap_net_raw_gid_count) {
			printk(KERN_INFO "grant_cap_net_raw: these group ids will be granted access: ");
			for(i = 0; i < grant_cap_net_raw_gid_count; i++) {
				if(i > 0)
					printk(KERN_CONT ", ");
				printk(KERN_CONT "%d", grant_cap_net_raw_gids[i]);
			}
			printk(KERN_CONT "\n");
		}
	}
	
	return 0;
}

static void grant_cap_net_raw_exit(void) {
	if(info_file)
		remove_proc_entry(GRANT_CAP_NET_RAW_PROCFS_INFO, NULL);
}

module_init(grant_cap_net_raw_init);
module_exit(grant_cap_net_raw_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Florian Schmidt <schmidt_florian@gmx.de>");
MODULE_DESCRIPTION("grant CAP_NET_RAW to every task that reads from /proc/grant_cap_net_raw");
